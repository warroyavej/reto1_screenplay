#Author: your.email@your.domain.com
#language: es
@Regresion
Característica: Traductor de Google
  Como usuario
  quiero ingresar al traductor de Google
  a traducir palabras entre diferentes lenguajes

  @Caso1
  Escenario: Traducir de Ingles a Español
    
    Dado Que Rafa quiere usar el traductor de Google
    Cuando yel traduce la palabra table de Inglés a Español
    Entonces el debería ver la palabra mesa en la pantalla
