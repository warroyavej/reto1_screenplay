#Author: warroyav@bancolombia.com
#language:es


@Regresion
Característica: Realizar el registro de un nuevo empleado
	Como usuario
	Quiero ingresar a la Web OrangeHR
	A realizar el registro de un nuevo empleado

	@Reto2
  Esquema del escenario: Registro de un nuevo empleado
    Dado que Wilson quiere acceder a la Web OrangeHR
    Cuando el realiza el ingreso del registro en la aplicación
    	|<Nombre>|<Nombre2>|<Apellidos>|<Id_Empleado>|<Locacion>|<Id_Empleado2>|<Cumpleaños>|<EstadoCivil2>|<Nacionalidad2>|<IdLicenciaConduccion>|<FechaVencLicencia>|<Usuario>|<ServMilitar>|
    Entonces el visualiza el nuevo empleado en el aplicativo
			|<Id_Empleado>|
    
    Ejemplos:
    |Nombre|Nombre2|Apellidos|Id_Empleado|Locacion|Id_Empleado2|Cumpleaños|EstadoCivil2|Nacionalidad2|IdLicenciaConduccion|FechaVencLicencia|Usuario|ServMilitar|
    |wilson|waj|Arroyave|025|Australian Regional HQ|0001|2015-07-02|Single|Colombian|1234|2005-02-08|prueba|activo|