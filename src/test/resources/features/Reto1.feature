#Author: warroyav@bancolombia.com
#language:es


@Regresion
Característica: Registro en la Web Automation Demo Site
	Como usuario
	Quiero ingresar a la Web Automation Demo Site
	A realizar el registro en la web

	@Reto1
  Esquema del escenario: Registro en la Web Automation Demo Site
    Dado que Wilson quiere acceder a la Web Automation Demo  Site
    Cuando el realiza el registro en la página
        |<Nombre>|<Apellido>|<Direccion>|<Email>|<Telefono>|<Idioma>|<Habilidades>|<Pais>|<Fecha_Año>|<Fecha_Mes>|<Fecha_Dia>|<Clave>|<Confirmar_Clave>|
    Entonces el verifica que se carga la pantalla con el texto Double Click on Edit Icon to EDIT the Table Row
    		|<texto_Validacion>|
    
    Ejemplos:
    |Nombre|Apellido|Direccion|Email|Telefono|Idioma|Habilidades|Pais|Fecha_Año|Fecha_Mes|Fecha_Dia|Clave|Confirmar_Clave|texto_Validacion|
    |wilson|Arroyave| calle 43|wjimenez@choucairtesting.com|3136561111|Spanish|APIs|Colombia|1980|October|18|Hola123|Hola123|- Double Click on Edit Icon to EDIT the Table Row.|