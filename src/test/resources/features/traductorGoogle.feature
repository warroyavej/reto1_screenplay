#Author: warroyav@bancolombia.com

@regresion
Feature: Traductor de Google
  Como usuario
  quiero ingresar al traductor de Google
  A traduccir palabras entre diferentes lenguajes

  @Caso1
  Scenario: Traducir de Ingles a Espanol

   Given Que Rafa quiere usar el Traductor de Google 
    When el traduce la palabra table de Ingles a Espanol
    Then el deberia ver la palabra mesa en la pantalla

#  @tag2
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#     | name1 |     5 | success |
#     | name2 |     7 | Fail    |
