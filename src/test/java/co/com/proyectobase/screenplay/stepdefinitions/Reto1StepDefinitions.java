package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta2;
import co.com.proyectobase.screenplay.tasks.AbrirReto1;
import co.com.proyectobase.screenplay.tasks.Registro;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto1StepDefinitions {
	
	@Managed(driver = "Chrome")
	private WebDriver hisBrowser;
	private Actor wilson = Actor.named("Wilson");
	
	@Before
	public void configuracionInicial()
	{
		wilson.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Wilson quiere acceder a la Web Automation Demo  Site$")
	public void queWisonQuiereAccederALaWebAutomationDemoSite() throws Exception {
		wilson.wasAbleTo(AbrirReto1.LaWebAutomatinDemoSite());
	}


	@Cuando("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(DataTable dtDatos) throws Exception {		
		wilson.attemptsTo(Registro.ElRealizaRegistroEnLaPagina(dtDatos));
		
	}

	@Entonces("^el verifica que se carga la pantalla con el texto Double Click on Edit Icon to EDIT the Table Row$")
	public void elVerificaQueSeCargaLaPantallaConElTextoDoubleClickOnEditIconToEDITTheTableRow(DataTable dtDatos2) throws Exception {
		wilson.should(seeThat(LaRespuesta2.es(),equalTo(dtDatos2.raw().get(0).get(0).trim())));
		
	}   
}
