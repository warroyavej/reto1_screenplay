package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta2;
import co.com.proyectobase.screenplay.questions.RespuestaValidarRegistro;
import co.com.proyectobase.screenplay.tasks.AbrirReto2Tasks;
import co.com.proyectobase.screenplay.tasks.LoginAplicativo;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto2_OrangeHRStepDefinition {
	
	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor wilson = Actor.named("Wilson");
	
	@Before
	public void configuracionInicial() {
		wilson.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Wilson quiere acceder a la Web OrangeHR$")
	public void queWilsonQuiereAccederALaWebOrangeHR() throws Exception {
		
		wilson.wasAbleTo(AbrirReto2Tasks.LaPaginaOrangeHR());		
	}

	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable dtDatos) throws Exception {
		
		wilson.attemptsTo(LoginAplicativo.LaWebOrangeHR(dtDatos));

	}

	@Entonces("^el visualiza el nuevo empleado en el aplicativo$")
	public void elVisualizaElNuevoEmpleadoEnElAplicativo(DataTable dtDatos2) throws Exception {
		wilson.should(seeThat(RespuestaValidarRegistro.es(),equalTo(dtDatos2.raw().get(0).get(0).trim())));
			
	}
}




