package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

@DefaultUrl("http://www.google.com")

public class GoogleHomePage extends PageObject{
	
	public static final Target btnAplicaciones = Target.the("Boton que muestra las aplicaciones de google").located(By.id("gbwa"));
	public static final Target btnGoogle_Traslate	 = Target.the("Bot�n de app traductor").located(By.id("gb51"));
	


}
