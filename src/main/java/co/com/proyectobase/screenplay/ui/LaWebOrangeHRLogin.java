package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("https://orangehrm-demo-6x.orangehrmlive.com/")

public class LaWebOrangeHRLogin extends PageObject{
	
	/*
	 * Mapeo del objeto para el logueo en la pagina.
	 */
	
	public static final Target btnLogin = Target.the("Boton de logueo en el aplicativo").located(By.id("btnLogin"));
}
