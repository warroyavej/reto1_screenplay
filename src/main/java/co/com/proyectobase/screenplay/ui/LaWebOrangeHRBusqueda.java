package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LaWebOrangeHRBusqueda extends PageObject {
	
	/*
	  * Mapeo de objetos para buscar y verificar el registro del usuario.
	  */
	public static final Target txtIdEmpleado = Target.the("Ingresar id del empleado").located(By.id("employeeId"));
	public static final Target btnListaEmpleados = Target.the("Ingresar a Lista de Empleados").located(By.xpath("//*[@id=\"menu_pim_viewEmployeeList\"]"));
	public static final Target txtBusqueda = Target.the("Ingresar Id del usuario a buscar").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target btnBuscar = Target.the("Click en el boton Buscar").located(By.id("quick_search_icon"));
	
}
