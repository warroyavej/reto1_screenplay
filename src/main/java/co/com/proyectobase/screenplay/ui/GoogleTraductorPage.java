package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.core.annotations.findby.By;

public class GoogleTraductorPage {
	

	public static final Target btnLenguaje_Origen = Target.the("Boton del idioma origen").located(By.id("gt-sl-gms"));
	public static final Target Opcion_Ingles = Target.the("La opcion Ingles").located(By.xpath("//div[@id='gt-sl-gms-menu']/table/tbody//tr/td//div[contains(text(),'ingl')]"));
	public static final Target btnLenguaje_Destino = Target.the("Boton del idioma destino").located(By.id("gt-tl-gms"));
	public static final Target Opcion_Espanol = Target.the("El segundo idioma").located(By.xpath("//div[@id='gt-tl-gms-menu']/table/tbody//tr/td//div[contains(text(),'espa')]"));
	public static final Target Area_de_Traduccion = Target.the("Lugar donde se escriben las palabras a traducir").located(By.id("source"));
	public static final Target btnTraducir = Target.the("El boton traducir").located(By.id("gt-submit"));
	public static final Target Area_Traducida = Target.the("Area donde se presenta la traduccion").located(By.id("gt-res-dir-ctr"));
	
}
