package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")

public class LaWebAutomationDemoSite extends PageObject {

	public static final Target txtNombre = Target.the("Campo del Nombre").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[1]/input"));
	public static final Target txtApellido = Target.the("Campo del Apellido").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[2]/input"));
	public static final Target txtDireccion = Target.the("Campo de la Direccion").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea"));
	public static final Target txtEmail = Target.the("Ingresar el email").located(By.xpath("//*[@id='eid']/input"));
	public static final Target txtTelefono = Target.the("Ingresar el numero telefonico").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[4]/div/input"));
	public static final Target rdbGenero = Target.the("Seleccionar el genero").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[1]/input"));
	public static final Target rdbAficiones = Target.the("Seleccionar las aficiones").located(By.xpath("//*[@id=\'checkbox2\']"));
	public static final Target txtIdiomas = Target.the("Seleccionar el idioma").located(By.xpath("//*[@id='msdd']"));
	public static final Target txtIdiomas2 = Target.the("Seleccionar el idioma").located(By.xpath("//*[@id='basicBootstrapForm']/div[7]/div/multi-select/div[2]/ul"));
	public static final Target txtHabilidad = Target.the("Seleccionar las Habilidades").located(By.xpath("//*[@id=\'Skills\']"));
	public static final Target txtPais = Target.the("Seleccionar un pais").located(By.xpath("//*[@id=\'countries\']"));
	public static final Target txtAño = Target.the("Seleccionar el año de Nacimiento").located(By.id("yearbox"));
	public static final Target txtMes = Target.the("Seleccionar el mes de Nacimiento").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select"));
	public static final Target txtDia = Target.the("Seleccionar el dia de Nacimientio").located(By.id("daybox"));
	public static final Target txtClave = Target.the("Ingresar la Clave").located(By.id("firstpassword"));
	public static final Target txtVerClave = Target.the("Verificar la Clave").located(By.id("secondpassword"));
	public static final Target btnEnviar = Target.the("Boton Enviar").located(By.id("submitbtn"));
	public static final Target lblValidacion_Registro = Target.the("Leer Texto luego del registro").located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
	
	
	
	
}
