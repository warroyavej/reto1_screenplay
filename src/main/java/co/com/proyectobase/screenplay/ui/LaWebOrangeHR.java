package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

//@DefaultUrl ("https://orangehrm-demo-6x.orangehrmlive.com/")

public class LaWebOrangeHR extends PageObject{
	
	/*
	 * Mapeo de Objetos para realizar el registro del empleado.
	 */
	public static final Target btnPim = Target.the("Desplegar opción PIM").located(By.xpath("//*[@id=\'menu_pim_viewPimModule\']/a/span[2]"));
	public static final Target btnAddEmpleado = Target.the("Opción de Adicionar Empleado").located(By.id("menu_pim_addEmployee"));
	public static final Target txtNombre = Target.the("Ingresar el Nombre del empleado").located(By.id("firstName"));
	public static final Target txtNombre2 = Target.the("Ingresar el segundo Nombre del empleado").located(By.id("middleName"));
	public static final Target txtApellidos = Target.the("Ingresar los apellidos del empleado").located(By.id("lastName"));
	public static final Target txtIdEmpleado = Target.the("Ingresar id del empleado").located(By.id("employeeId"));
	public static final Target txtLocalizacion = Target.the("Seleccionar la localización").located(By.id("location_inputfileddiv"));
	public static final Target txtLocalizacion2 = Target.the("Seleccionar la localización del empleado").located(By.id("location_inputfileddiv"));
	public static final Target btnGuardar = Target.the("Click en el boton Guardar").located(By.id("systemUserSaveBtn"));
	
	/*
	 * Datos segundo formulario
	 */
	
	public static final Target txtIdEmpleado2 = Target.the("Ingresar otro ID").located(By.id("other_id"));
	public static final Target txtCumpleaños = Target.the("Ingresar la fecha de cumpleaños").located(By.id("date_of_birth"));
	public static final Target lstEstadoCivil = Target.the("Seleccionar el campo estado civil").located(By.id("marital_status_inputfileddiv"));
	public static final Target lstEstadoCivil2 = Target.the("Seleccionar el estado civil").located(By.id("marital_status_inputfileddiv"));
	public static final Target chkGenero = Target.the("Seleccionar el genero").located(By.xpath("//*[@id=\'pimPersonalDetailsForm\']/materializecss-decorator[3]/div/sf-decorator[3]/div/ul"));
	public static final Target lstNacionalidad = Target.the("Seleccionar campo nacionalidad").located(By.id("nationality_inputfileddiv"));
	public static final Target lstNacionalidad2 = Target.the("Seleccionar la nacionalidad").located(By.id("nationality_inputfileddiv"));
	public static final Target txtIdLicenciaConduccion = Target.the("Ingresar el numero de la licencia de conduccion").located(By.id("driver_license"));
	public static final Target txtFechaVenLicencia = Target.the("Ingresar la fecha de vencimiento de la licencia").located(By.id("license_expiry_date"));
	public static final Target txtUsuario = Target.the("Ingresar el usuario").located(By.id("nickName"));
	public static final Target txtServMilitar = Target.the("Ingresar Servicio Militar").located(By.id("militaryService"));
	public static final Target chkFumador = Target.the("Seleccionar si fuma o no").located(By.xpath("//*[@id=\'pimPersonalDetailsForm\']/materializecss-decorator[5]/div/sf-decorator[3]/div"));
	public static final Target btnGuardar2 = Target.the("Click en el boton Guardar").located(By.xpath("//*[@id=\'pimPersonalDetailsForm\']/materializecss-decorator[8]/div/sf-decorator[2]/div/button"));
	public static final Target lblValidacion_Registro = Target.the("Validar usuario registrado").located(By.xpath("//*[@id=\'employeeListTable\']/tbody/tr[1]/td[2]"));
//	public static final Target lblValidacion_Registro = Target.the("Validar usuario registrado").located(By.id("angucomplete-title-temp-id"));

}
