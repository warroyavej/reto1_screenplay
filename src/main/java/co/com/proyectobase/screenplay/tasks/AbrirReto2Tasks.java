package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.LaWebOrangeHR;
import co.com.proyectobase.screenplay.ui.LaWebOrangeHRLogin;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirReto2Tasks implements Task{
	
	private LaWebOrangeHRLogin laWebOrangeHRLogin;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(laWebOrangeHRLogin));
	}

	public static AbrirReto2Tasks LaPaginaOrangeHR() {
		return Tasks.instrumented(AbrirReto2Tasks.class);
	}
	

}
