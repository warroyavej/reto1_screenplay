package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.LaWebAutomationDemoSite;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirReto1 implements Task{
	
	private LaWebAutomationDemoSite laWebAutomationDemoSite;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(laWebAutomationDemoSite));
		
	}

	public static AbrirReto1 LaWebAutomatinDemoSite() {
		return Tasks.instrumented(AbrirReto1.class);
	}
	

}
