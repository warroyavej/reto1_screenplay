package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.GoogleHomePage;
import co.com.proyectobase.screenplay.ui.GoogleTraductorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Traducir implements Task {
	
	private String palabra;
	
	public Traducir(String palabra) {
		super();
		this.palabra = palabra;
	}	

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(GoogleHomePage.btnAplicaciones));
		actor.attemptsTo(Click.on(GoogleHomePage.btnGoogle_Traslate));		
		actor.attemptsTo(Click.on(GoogleTraductorPage.btnLenguaje_Origen));
		actor.attemptsTo(Click.on(GoogleTraductorPage.Opcion_Ingles));
		actor.attemptsTo(Click.on(GoogleTraductorPage.btnLenguaje_Destino));
		actor.attemptsTo(Click.on(GoogleTraductorPage.Opcion_Espanol));
		actor.attemptsTo(Enter.theValue(palabra).into(GoogleTraductorPage.Area_de_Traduccion));
		actor.attemptsTo(Click.on(GoogleTraductorPage.btnTraducir));
		
		
	}

	public static Traducir DeInglesAEspanol(String palabra) {
		return Tasks.instrumented(Traducir.class, palabra);
	}
}
