package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebElement;
import co.com.proyectobase.screenplay.ui.LaWebAutomationDemoSite;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


public class Registro implements Task{
	
	private DataTable dtDatos;
	
	
	public Registro(DataTable dtDatos) {
		this.dtDatos = dtDatos;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		List<List<String>> data = dtDatos.raw();

		actor.attemptsTo(Enter.theValue(data.get(0).get(0).trim()).into(LaWebAutomationDemoSite.txtNombre));
		actor.attemptsTo(Enter.theValue(data.get(0).get(1).trim()).into(LaWebAutomationDemoSite.txtApellido));
		actor.attemptsTo(Enter.theValue(data.get(0).get(2).trim()).into(LaWebAutomationDemoSite.txtDireccion));
		actor.attemptsTo(Enter.theValue(data.get(0).get(3).trim()).into(LaWebAutomationDemoSite.txtEmail));
		actor.attemptsTo(Enter.theValue(data.get(0).get(4).trim()).into(LaWebAutomationDemoSite.txtTelefono));
		actor.attemptsTo(Click.on(LaWebAutomationDemoSite.rdbGenero));
		actor.attemptsTo(Click.on(LaWebAutomationDemoSite.rdbAficiones));
		
		actor.attemptsTo(Click.on(LaWebAutomationDemoSite.txtIdiomas));
		clic_objeto_lista(LaWebAutomationDemoSite.txtIdiomas2.resolveFor(actor),data.get(0).get(5).trim());
		
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).get(6).trim()).from(LaWebAutomationDemoSite.txtHabilidad));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).get(7).trim()).from(LaWebAutomationDemoSite.txtPais));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).get(8).trim()).from(LaWebAutomationDemoSite.txtAño));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).get(9).trim()).from(LaWebAutomationDemoSite.txtMes));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).get(10).trim()).from(LaWebAutomationDemoSite.txtDia));
		actor.attemptsTo(Enter.theValue(data.get(0).get(11).trim()).into(LaWebAutomationDemoSite.txtClave));
		actor.attemptsTo(Enter.theValue(data.get(0).get(12).trim()).into(LaWebAutomationDemoSite.txtVerClave));
		actor.attemptsTo(Click.on(LaWebAutomationDemoSite.btnEnviar));
		
	}
		
		public void clic_objeto_lista(WebElementFacade objeto, String data) {
			List<WebElement> listElementos = objeto.findElements(By.tagName("li"));
			for (WebElement e : listElementos) {
            	if (e.getText().equals(data)) {
            		e.click();
            		break;
            }
            	}
			}

	public static Registro ElRealizaRegistroEnLaPagina(DataTable dtDatos) {
		return Tasks.instrumented(Registro.class, dtDatos);
		
	}

}