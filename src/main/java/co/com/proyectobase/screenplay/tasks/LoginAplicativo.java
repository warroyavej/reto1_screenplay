package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.interactions.RecorrerListas;
import co.com.proyectobase.screenplay.ui.LaWebOrangeHR;
import co.com.proyectobase.screenplay.ui.LaWebOrangeHRBusqueda;
import co.com.proyectobase.screenplay.ui.LaWebOrangeHRLogin;
import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class LoginAplicativo implements Task {
	
	private DataTable dtDatos;
	
	public LoginAplicativo(DataTable dtDatos) {
		this.dtDatos = dtDatos;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		List<List<String>> data = dtDatos.raw();
		/*
		 * Login en la pagina OrangeHR
		 */

		actor.attemptsTo(Click.on(LaWebOrangeHRLogin.btnLogin));
		
		/*
		 * Datos Primer Formulario
		 */
		actor.attemptsTo(Click.on(LaWebOrangeHR.btnPim));
		actor.attemptsTo(Click.on(LaWebOrangeHR.btnAddEmpleado));
		actor.attemptsTo(Enter.theValue(data.get(0).get(0).trim()).into(LaWebOrangeHR.txtNombre));
		actor.attemptsTo(Enter.theValue(data.get(0).get(1).trim()).into(LaWebOrangeHR.txtNombre2));
		actor.attemptsTo(Enter.theValue(data.get(0).get(2).trim()).into(LaWebOrangeHR.txtApellidos));
		actor.attemptsTo(Enter.theValue(data.get(0).get(3).trim()).into(LaWebOrangeHR.txtIdEmpleado));
		actor.attemptsTo(Click.on(LaWebOrangeHR.txtLocalizacion));
		actor.attemptsTo(RecorrerListas.Desde(LaWebOrangeHR.txtLocalizacion2,data.get(0).get(4).trim()));
		actor.attemptsTo(Click.on(LaWebOrangeHR.btnGuardar));
		/*
		 * Datos del segundo formulario
		 */
		actor.attemptsTo(Enter.theValue(data.get(0).get(5).trim()).into(LaWebOrangeHR.txtIdEmpleado2));
		actor.attemptsTo(Enter.theValue(data.get(0).get(6).trim()).into(LaWebOrangeHR.txtCumpleaños));
		actor.attemptsTo(Click.on(LaWebOrangeHR.lstEstadoCivil));
		actor.attemptsTo(RecorrerListas.Desde(LaWebOrangeHR.lstEstadoCivil2,data.get(0).get(7).trim()));
		actor.attemptsTo(Click.on(LaWebOrangeHR.lstNacionalidad));
		actor.attemptsTo(RecorrerListas.Desde(LaWebOrangeHR.lstNacionalidad2,data.get(0).get(8).trim()));
		actor.attemptsTo(Enter.theValue(data.get(0).get(9).trim()).into(LaWebOrangeHR.txtIdLicenciaConduccion));
		actor.attemptsTo(Enter.theValue(data.get(0).get(10).trim()).into(LaWebOrangeHR.txtFechaVenLicencia));
		actor.attemptsTo(Enter.theValue(data.get(0).get(11).trim()).into(LaWebOrangeHR.txtUsuario));
		actor.attemptsTo(Enter.theValue(data.get(0).get(12).trim()).into(LaWebOrangeHR.txtServMilitar));
		actor.attemptsTo(Click.on(LaWebOrangeHR.chkFumador));
		actor.attemptsTo(Click.on(LaWebOrangeHR.btnGuardar2));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
		/*
		 * Objetos para la busqueda
		 */
		actor.attemptsTo(Click.on(LaWebOrangeHRBusqueda.btnListaEmpleados));
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
		
		actor.attemptsTo(Enter.theValue(data.get(0).get(3).trim()).into(LaWebOrangeHRBusqueda.txtBusqueda));
		actor.attemptsTo(Click.on(LaWebOrangeHRBusqueda.btnBuscar));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void clic_objeto_lista(WebElementFacade objeto, String data) {
		List<WebElement> listElementos = objeto.findElements(By.tagName("li"));
		for (WebElement e : listElementos) {
        	if (e.getText().equals(data)) {
        		e.click();
        		break;
        }
        	}
		}

	public static LoginAplicativo LaWebOrangeHR(DataTable dtDatos) {
		return Tasks.instrumented(LoginAplicativo.class, dtDatos);
				
	}

}
