package co.com.proyectobase.screenplay.interactions;

import java.util.List;


import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.targets.Target;

public class RecorrerListas implements Interaction{
	
	private Target ubicacion;
	private String localizacion;

	public RecorrerListas(Target ubicacion, String localizacion) {
		this.ubicacion = ubicacion;
		this.localizacion = localizacion;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		List<WebElement> listObjeto = ubicacion.resolveFor(actor).findElements(By.tagName("span"));
			for(int i=0; i < listObjeto.size(); i++) {
				if (listObjeto.get(i).getText().contains(localizacion)) {
					listObjeto.get(i).click();
					break;
				}
			}
		
	}

	public static Performable Desde(Target ubicacion, String localizacion) {
		
		return new RecorrerListas(ubicacion, localizacion);
	}
	

}
