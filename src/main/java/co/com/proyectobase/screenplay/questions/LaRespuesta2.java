package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.LaWebAutomationDemoSite;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta2 implements Question<String> {

	public static LaRespuesta2 es() {

		return new LaRespuesta2();
	}

	@Override
	public String answeredBy(Actor actor) {

		return Text.of(LaWebAutomationDemoSite.lblValidacion_Registro).viewedBy(actor).asString();
	}
}