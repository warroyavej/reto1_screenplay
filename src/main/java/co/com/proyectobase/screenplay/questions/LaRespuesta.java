package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.GoogleTraductorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String> {

	public static LaRespuesta es() {

		return new LaRespuesta();
	}

	@Override
	public String answeredBy(Actor actor) {

//		String retorno = "";
//		
//		if (actor.getName().compareTo("wilson")==0) {
		return Text.of(GoogleTraductorPage.Area_Traducida).viewedBy(actor).asString();
//		}
//		else { 	
//		retorno = Text.of(LaWebAutomationDemoSite.lblValidacion_Registro).viewedBy(actor).asString();
//		}
//		return retorno;
	}

}
