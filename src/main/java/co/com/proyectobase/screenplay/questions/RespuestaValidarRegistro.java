package co.com.proyectobase.screenplay.questions;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.ui.LaWebOrangeHR;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class RespuestaValidarRegistro implements Question<String> {
	
	public static RespuestaValidarRegistro es() {
		
		return new RespuestaValidarRegistro();
		
	}

	@Override
	public String answeredBy(Actor actor) {
		ImprimirEnConsola();
		return Text.of(LaWebOrangeHR.lblValidacion_Registro).viewedBy(actor).asString();
		}
	
	public void ImprimirEnConsola() {
		System.out.println("USUARIO REGISTRADO EXITOSAMENTE¡¡¡¡¡¡");
	}

}
